#include <stdio.h>
#include <sys/socket.h>
#include <dirent.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <pwd.h>
#include <dirent.h>
#include <fcntl.h>
#include <errno.h>
#include <time.h>
#include <sys/wait.h>
#include<pthread.h>
#include<sys/types.h>
# define HOME "/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3"

pthread_t tid1, tid2, tid3, tid4, tid5;
pid_t child;
int statusz;

void download_drive(char *id, char *save){
    char *link = malloc(100);
    strcpy(link, "https://docs.google.com/uc?export=download&id=");
    strcat(link, id);
   
    pid_t pid = fork();
    int status;
    if (pid == 0){
        char *argv[] = {"wget", "--quiet", "--no-check-certificate", link, "-O", save, NULL};
        execv("/opt/homebrew/bin/wget", argv);
        exit(0);
    }
    wait(&status);
}


void extract_file(char *zippath, char *extractpath){
    pid_t pid = fork();
    int status;
    if (pid == 0){
        char *argv[] = {"unzip", "-q", zippath, "-d", extractpath, NULL};
        execv("/usr/bin/unzip", argv);
        exit(0);
    }
    wait(&status);
}


void decode_file(char *filename, char *output, char *final_output){
    pid_t pid = fork();
    int status;

    if (pid == 0){
        char *argv[] = {"base64", "-d","-i", filename, "-o",output, NULL};
        execv("/usr/bin/base64", argv);
    }
    waitpid(pid, NULL, 0);
    FILE *filetxt;
    FILE *fp;
    char *konten = (char*)malloc(100);
    filetxt = fopen(final_output, "a");
    fp = fopen(output , "r");
    if(filetxt == NULL){
        exit(EXIT_FAILURE); // Jika gagal membuat proses baru, program akan berhenti
    }
    fgets(konten,100,fp);
    fputs(konten,filetxt);
    fprintf(filetxt,"\n");
    fclose(fp); fclose(filetxt);
}

void createDir(char route[]){
    pid_t pid = fork();
    int status;
    if (pid == 0){
        execlp("mkdir", "mkdir", "-p", route, NULL);
    }
    waitpid(pid,NULL,0);
}

void* download(void *arg){
    statusz = 0;
    unsigned long i=0;
	pthread_t id=pthread_self();
	if(pthread_equal(id,tid1)) //download
	{
        download_drive("1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1", HOME "/music.zip");
        download_drive("1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt", HOME "/quote.zip");   
	}
    statusz=1;
    return NULL;
}

void* extract(void *arg){

    while(statusz != 1)
    {

    }

    unsigned long i=0;
    pthread_t id=pthread_self();
    if(pthread_equal(id,tid2)) //extract
    {

        createDir("music");
        extract_file(HOME "/music.zip", "music");

        
    }else if(pthread_equal(id,tid3)) //extract
    {
        createDir("quote");
        extract_file(HOME "/quote.zip", "quote");
        
    }
    return NULL;
}

void* decode(void *arg){
    unsigned long i=0;
	pthread_t id=pthread_self();
	if(pthread_equal(id,tid4)) //decode 1
	{
        for(int i=0 ; i<9 ; i++){
   
            char *file_name = (char*) malloc(100);
            char *nomor = (char*)malloc(100); //convert nomor to string
            char *output_name = (char*)malloc(100); //initialize output name
            sprintf(nomor,"%d",i+1); 

            // full path copied to file_name -- file_name/m -- file_name/m1
            strcpy(file_name,"/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3/music/"); strcat(file_name,"m"); strcat(file_name,nomor); 
            
            // file_name copied to output_name
            strcpy(output_name,file_name);

            // output_name.txt -- file_name/m1.txt
            strcat(file_name,".txt");

            // file_name/m11 -- file_name/m11.txt
            strcat(output_name,nomor);strcat(output_name,".txt");
            decode_file(file_name,output_name,"/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3/music/music.txt");
        }
       
	}else if (pthread_equal(id,tid5)){ //decode 2
        for(int i=0 ; i<9 ; i++){
            char *file_name = (char*) malloc(100);
            char *nomor = (char*) malloc(100);
            char *output_name = (char*)malloc(100);
            sprintf(nomor,"%d",i+1);
            strcpy(file_name,"/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3/quote/"); strcat(file_name,"q"); strcat(file_name,nomor); 
            strcpy(output_name,file_name);
            strcat(file_name,".txt");
            strcat(output_name,nomor);strcat(output_name,".txt");
            decode_file(file_name,output_name, "/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3/quote/quote.txt");
        }
    } 
    return NULL;
}

int main(void){

	pthread_create(&(tid1), NULL, &download, NULL); //Download zip
    pthread_create(&(tid2), NULL, &extract, NULL); //Extract music
    pthread_create(&(tid3), NULL, &extract, NULL); //Extract quote

    pthread_join(tid1, NULL);
    pthread_join(tid2, NULL);
    pthread_join(tid3, NULL);

    pthread_create(&(tid4), NULL, &decode, NULL); //decode 1
    pthread_create(&(tid5), NULL, &decode, NULL); //decode 2
    pthread_join(tid4, NULL);
    pthread_join(tid5, NULL);

    createDir("/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3/hasil");
    rename("/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3/music/music.txt", "/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3/hasil/music.txt");
    rename("/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3/quote/quote.txt", "/Users/ingwerludwig/DATA/College/4TH\ TERM/OPERATING\ SYSTEM/PRAKTIKUM\ 3/hasil/quote.txt");
	
    pid_t pid = fork();
    if(pid == 0){
        char* argv[] = {"zip","-P","mihinomenestingwer","-r","hasil.zip","hasil",NULL};
        execv("/usr/bin/zip", argv);
    }
    waitpid(pid,NULL,0);
    
    return 0;
}